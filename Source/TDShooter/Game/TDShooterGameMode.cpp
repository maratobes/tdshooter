// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDShooterGameMode.h"
#include "TDShooterPlayerController.h"
#include "../Character/TDShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDShooterGameMode::ATDShooterGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDShooterPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}